package org.hl.mall.pay;

import org.hl.mall.pay.common.swagger.annotation.EnableHLSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author xiaochangbai
 * @since 2022/5/12 15:24
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableHLSwagger2
@EnableFeignClients("org.hl")
public class HlMallPayApplication {


    public static void main(String[] args) {
        SpringApplication.run(HlMallPayApplication.class,args);
    }


}
