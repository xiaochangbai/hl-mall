package org.hl.mall.pay;

import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Price;
import com.stripe.param.PriceCreateParams;
import org.junit.Test;

/**
 * @author xiaochangbai
 * @since 2022/5/12 17:18
 */
public class StripeTest {

    /**
     * 创建一个简单的产品
     * @throws StripeException
     */
    @Test
    public void test() throws StripeException {
        // Set your secret key. Remember to switch to your live secret key in production.
        // See your keys here: https://dashboard.stripe.com/apikeys
        Stripe.apiKey = "sk_test_51KwHLZJCN13SSpCt4v8idGoDY7brBjYmQhMHqc0dBmcZ1JBnwSUSdV8M05n1m7OfVTHycbsLMiCfvtMJJAHxvJTy00XjCVhoV7";

        PriceCreateParams params =
                PriceCreateParams
                        .builder()
                        .setCurrency("usd")
                        .setUnitAmount(120000L)
                        .setProductData(
                                PriceCreateParams.ProductData
                                        .builder()
                                        .setName("stand up paddleboard")
                                        .build()
                        )
                        .build();

        Price price = Price.create(params);
        System.out.println(price);
    }

}
