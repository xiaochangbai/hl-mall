package org.hl.mall.auth.data.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.hl.mall.auth.data.domain.Member;


/**
 * 会员数据处理层
 *
 * @author Bulbasaur
 * @since 2020-02-25 14:10:16
 */
public interface MemberMapper extends BaseMapper<Member> {


}