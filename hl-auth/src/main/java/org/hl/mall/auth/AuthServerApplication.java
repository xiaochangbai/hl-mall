package org.hl.mall.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthServerApplication.class, args);
	}
	/**
	 * -javaagent:D:\apache\apache-skywalking-apm-es7-8.4.0\apache-skywalking-apm-bin-es7\agent\skywalking-agent.jar
	 * -Dskywalking.agent.service_name=tulingmall-authcenter
	 * -Dskywalking.collector.backend_service=192.168.65.232:11800
	 *
	 * -javaagent:D:\programs\apps\Distributed\skywalking\skywalking-agent\skywalking-agent.jar
	 * -Dskywalking.agent.service_name=tulingmall-gateway
	 * -Dskywalking.collector.backend_service=127.0.0.1:11800
	 *
	 * docker run --name skywalking-oap -p 11800:11800 -p 1234:1234  -p 12800:12800 -d apache/skywalking-oap-server
	 * docker run --name skywalking-oap-ui -p 8080:8080 -d -e SW_OAP_ADDRESS=http://172.16.8.107:12800 apache/skywalking-ui
	 */

	/**
	 * 密码模式
	 * http://localhost:8888/oauth/token?username=windy&password=123456&grant_type=password&client_id=portal_app&client_secret=123123&scope=read
	 */

}
