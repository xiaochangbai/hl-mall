package org.hl.mall.auth.data.domain;


import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.util.Date;

/**
 * 会员
 */
@Data
@ApiModel(value = "会员")
@TableName("ums_member")
@NoArgsConstructor
public class Member{

    private static final long serialVersionUID = 1L;

    private Long id;

    @ApiModelProperty(value = "会员用户名")
    private String username;

    @ApiModelProperty(value = "会员密码")
    private String password;


    public Member(String username, String password) {
        this.username = username;
        this.password = password;
    }




}
