## 红灵商城 B2B2C商城系统


### 简介

 **系统全端全部代码开源**

hl-mall 是基于SpringCloud Alibaba微服务技术栈构建的一整套全栈开源商城项目,涉及 后端微服务、 前端管理 、 微信小程序和 APP应用 等多端的开发。

前后端分离，支持分布式部署，支持Docker，各个API独立，并且有独立的消费者。


### 架构图
<a href="https://www.processon.com/diagraming/60094f4e5653bb510892a3a8" target="_blank">

![系统架构图](docs/images/structure.jpg)

</a>


### 开发/使用/常见问题 帮助文档
