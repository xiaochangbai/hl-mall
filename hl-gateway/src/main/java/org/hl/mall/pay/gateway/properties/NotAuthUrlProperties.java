package org.hl.mall.pay.gateway.properties;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedHashSet;


/**
 * 网关配置
 */
@Data
@Configuration
@ConfigurationProperties("hl.mall.gateway")
public class NotAuthUrlProperties {

    private LinkedHashSet<String> shouldSkipUrls;

}
