package org.hl.mall.pay.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author xiaochangbai
 * @since 2022/5/10 14:07
 */
@EnableDiscoveryClient
@SpringBootApplication
public class HlMallGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(HlMallGatewayApplication.class,args);
    }

}
