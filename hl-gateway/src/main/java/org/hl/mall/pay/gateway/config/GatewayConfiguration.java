package org.hl.mall.pay.gateway.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.hl.mall.pay.gateway.filter.PigRequestGlobalFilter;
import org.hl.mall.pay.gateway.filter.PigSpringDocGlobalFilter;
import org.hl.mall.pay.gateway.handler.GlobalExceptionHandler;
import org.hl.mall.pay.gateway.handler.ImageCodeHandler;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 网关配置
 */
@Configuration(proxyBeanMethods = false)
public class GatewayConfiguration {

	@Bean
	public PigRequestGlobalFilter pigRequestGlobalFilter() {
		return new PigRequestGlobalFilter();
	}

	@Bean
	public PigSpringDocGlobalFilter pigSwaggerGlobalFilter() {
		return new PigSpringDocGlobalFilter();
	}



	@Bean
	public GlobalExceptionHandler globalExceptionHandler(ObjectMapper objectMapper) {
		return new GlobalExceptionHandler(objectMapper);
	}

	@Bean
	public ImageCodeHandler imageCodeHandler() {
		return new ImageCodeHandler();
	}

}
