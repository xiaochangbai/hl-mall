package org.hl.common.aop.annotation;

import java.lang.annotation.*;

/**
 * 演示站点注解
 * <p>
 * PS 此注解需要用户登录之后才可以使用
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface DemoSite {


}
