package org.hl.common.utils;


import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;


/**
 * ip工具
 */
@Slf4j
@Component
public class IpHelper {

    /**
     * qq lbs 地区查询key
     */
    @Value("${lili.lbs.key}")
    private String key;
    /**
     * qq lbs 地区查询key
     */
    @Value("${lili.lbs.sk}")
    private String sk;

    private static final String API = "https://apis.map.qq.com";


    /**
     * 获取IP返回地理信息
     *
     * @param request 请求参数
     * @return 城市信息
     */
    public String getIpCity(HttpServletRequest request) {

        String url = "/ws/location/v1/ip?key=" + key + "&ip=" + IpUtils.getIpAddress(request);
        String sign = SecureUtil.md5(url + sk);
        url = API + url + "&sign=" + sign;
        String result = "未知";
        try {
            String json = HttpUtil.get(url, 3000);
            JSONObject jsonObject = JSONObject.parseObject(json);
            String status = jsonObject.getString("status");
            if ("0".equals(status)) {
                JSONObject address = jsonObject.getJSONObject("result").getJSONObject("ad_info");
                String nation = address.getString("nation");
                String province = address.getString("province");
                String city = address.getString("city");
                String district = address.getString("district");
                if (StrUtil.isNotBlank(nation) && StrUtil.isBlank(province)) {
                    result = nation;
                } else {
                    result = province;
                    if (StrUtil.isNotBlank(city)) {
                        result += " " + city;
                    }
                    if (StrUtil.isNotBlank(district)) {
                        result += " " + district;
                    }
                }
            }
        } catch (Exception e) {
            log.info("获取IP地理信息失败");
        }
        return result;
    }
}
