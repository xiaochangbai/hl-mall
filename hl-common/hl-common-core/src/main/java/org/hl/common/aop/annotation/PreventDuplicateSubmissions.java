package org.hl.common.aop.annotation;

import java.lang.annotation.*;

/**
 * 防止重复提交注解
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface PreventDuplicateSubmissions {


    /**
     * 过期时间
     */
    long expire() default 3;
}
