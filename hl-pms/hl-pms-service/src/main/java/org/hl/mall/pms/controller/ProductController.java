package org.hl.mall.pms.controller;

import org.hl.mall.oms.OrderApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xiaochangbai
 * @since 2022/5/10 16:04
 */
@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private OrderApi orderApi;

    @GetMapping("/list")
    @ResponseBody
    public Object list(){
        Object list = orderApi.list();
        return list;
    }

}
