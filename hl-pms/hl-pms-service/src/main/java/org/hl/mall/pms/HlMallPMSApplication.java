package org.hl.mall.pms;

import org.hl.mall.pay.common.swagger.annotation.EnableHLSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author xiaochangbai
 * @since 2022/5/10 15:59
 */

@SpringBootApplication
@EnableDiscoveryClient
@EnableHLSwagger2
@EnableFeignClients("org.hl.mall")
public class HlMallPMSApplication {

    public static void main(String[] args) {
        SpringApplication.run(HlMallPMSApplication.class,args);
    }

}
