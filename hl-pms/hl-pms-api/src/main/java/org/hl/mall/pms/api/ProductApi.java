package org.hl.mall.pms.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@FeignClient("hl-pms")
public interface ProductApi {

    @GetMapping("/product/list")
    @ResponseBody
    public String list();

}
