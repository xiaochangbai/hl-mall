package org.hl.mall.oms.controller;

import org.hl.common.enums.ResultUtil;
import org.hl.mall.pms.api.ProductApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xiaochangbai
 * @since 2022/5/10 16:04
 */
@RequestMapping("/order")
@RestController
public class OrderController {

    @Autowired
    private ProductApi productApi;


    @GetMapping("/list")
    @ResponseBody
    public Object list(){
        Map<String,Object> data = new HashMap<>();
        data.put("name","macbook pro 14");
        data.put("price",15000);
        return ResultUtil.data(data);
    }

}
