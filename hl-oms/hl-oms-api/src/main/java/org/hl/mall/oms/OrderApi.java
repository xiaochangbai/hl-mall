package org.hl.mall.oms;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@FeignClient("hl-oms")
public interface OrderApi {

    @GetMapping("/order/list")
    @ResponseBody
    Object list();


}
